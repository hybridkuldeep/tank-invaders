
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import "pixi";
import "p2";
import * as Phaser from "phaser-ce";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  public game: Phaser.Game;
  public state: any;
  public level = 1;

  public player;
  public aliens;
  public bullets;
  public bulletTime = 0;
  public cursors: any;
  public fireButton;
  public explosions;
  public starfield;
  public score = 0;
  public scoreString = '';
  public scoreText;
  public lives;
  public enemyBullet;
  public firingTimer = 0;
  public stateText;
  public livingEnemies: any = [];
  enemyBullets
  public keepFireBulletInterval;
  public keepFireing = false;
  public keepPlayerMovingInterval;
  public playerIsMoveLeft;

  public explosionSound;
  public fireSound;
  constructor(public navCtrl: NavController) {

  }  

  ionViewDidLoad = () => {
    this.start();
  }
  start(){
    this.state = {
      init: this.init,
      preload: this.preload,
      create: this.create,
      update: this.update,
      render: this.render
    };
    this.game = new Phaser.Game('100%', '98%', Phaser.CANVAS, "game", this.state);
    console.log(this.game);
  }

  init = () => {
    let text = "Phaser Version " + Phaser.VERSION + " works!";
    console.log(text);
    console.log("INIT FUNCTION")
  }

  preload = () => { 
    this.game.load.image('bullet', 'assets/bullet.png');
    this.game.load.image('enemyBullet', 'assets/enemy-bullet.png');
    this.game.load.spritesheet('invader', 'assets/invader32x32x4.png', 32, 32);
    this.game.load.image('ship', 'assets/player.png');
    this.game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    this.game.load.image('starfield', 'assets/starfield.png');
    this.game.load.image('background', 'assets/background2.png');
    this.game.load.audio('explosion', 'assets/explosion.mp3');
    this.game.load.audio('fire', 'assets/blaster.mp3');
    console.log("PRELOAD FUNCTION")
  }

  
   create = ()=> {
    this.game.physics.startSystem(Phaser.Physics.ARCADE);

    //  The scrolling starfield background
    this.starfield = this.game.add.tileSprite(0, 0, this.game.world.width, this.game.world.height, 'starfield');

    //  Our bullet group
    this.bullets = this.game.add.group();
    this.bullets.enableBody = true;
    this.bullets.physicsBodyType = Phaser.Physics.ARCADE;
    this.bullets.createMultiple(30, 'bullet');
    this.bullets.setAll('anchor.x', 0.5);
    this.bullets.setAll('anchor.y', 1);
    this.bullets.setAll('outOfBoundsKill', true);
    this.bullets.setAll('checkWorldBounds', true);

    // The enemy's bullets
   this.enemyBullets = this.game.add.group();
   this.enemyBullets.enableBody = true;
   this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
   this.enemyBullets.createMultiple(30, 'enemyBullet');
   this.enemyBullets.setAll('anchor.x', 0.5);
   this.enemyBullets.setAll('anchor.y', 1);
   this.enemyBullets.setAll('outOfBoundsKill', true);
   this.enemyBullets.setAll('checkWorldBounds', true);

    //  The hero!
    this.player = this.game.add.sprite(this.game.world.centerX, this.game.world.height-50, 'ship');
    this.player.anchor.setTo(0.5, 0.5);
    this.game.physics.enable(this.player, Phaser.Physics.ARCADE);
    this.player.checkWorldBounds = true;
    this.player.events.onOutOfBounds.add(this.playerOutOfBounds, this);
    //  The baddies!
    this.aliens = this.game.add.group();
    this.aliens.enableBody = true;
    this.aliens.physicsBodyType = Phaser.Physics.ARCADE;

    this.createAliens();
    

    //  The score
    this.scoreString = 'Score : ';
    this.scoreText = this.game.add.text(10, 10, this.scoreString + this.score, { font: '20px Arial', fill: '#fff' });

    //  Lives
    this.lives = this.game.add.group();
    this.game.add.text(this.game.world.width - 170, 10, 'Lives : ', { font: '20px Arial', fill: '#fff' });

    //  Text
    this.stateText = this.game.add.text(this.game.world.centerX,this.game.world.centerY,' ', { font: '30px Arial', fill: '#fff' });
    this.stateText.anchor.setTo(0.5, 0.5);
    this.stateText.visible = false;

    for (var i = 0; i < 3; i++) 
    {
        var ship = this.lives.create(this.game.world.width - 100 + (30 * i), 20, 'ship');
        ship.anchor.setTo(0.5, 0.5);
        ship.angle = 90;
        ship.alpha = 0.8;
    }

    //  An explosion pool
    this.explosions = this.game.add.group();
    this.explosions.createMultiple(30, 'kaboom');
    this.explosions.forEach(this.setupInvader, this);

    //  And some controls to play the this.game with
    this.cursors = this.game.input.keyboard.createCursorKeys();
    this.fireButton = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    //sound effects 
    this.explosionSound = this.game.add.audio('explosion');
    this.fireSound = this.game.add.audio('fire');
    
}

createAliens () {
    //increase aliens row when level increase but only 2
    let level = this.level;
    if(level > 2){
        level = 2
    }
    for (var y = 0; y < (3+level); y++)
    {
        for (var x = 0; x < 10; x++)
        {
            var alien = this.aliens.create(x * 30, y * 50, 'invader');
            alien.anchor.setTo(0.5, 0.5);
            alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
            alien.play('fly');
            alien.body.moves = false;
        }
    }

    this.aliens.x = -30;
    this.aliens.y = 50;

    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    let tween = this.game.add.tween(this.aliens).to( { x: 200 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);

    //  When the tween loops it calls descend
    tween.onLoop.add(this.descend, this);
}

 setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

 descend() {

  this.aliens.y += 10;

}

 update = ()  => {

    //  Scroll the background
    this.starfield.tilePosition.y += 2;

    if (this.player.alive)
    {
        //  Reset the this.player, then check for movement keys
        this.player.body.velocity.setTo(0, 0);

        if (this.cursors.left.isDown)
        {
            this.player.body.velocity.x = -200;
        }
        else if (this.cursors.right.isDown)
        {
            this.player.body.velocity.x = 200;
        }

        
        //  Firing?
        if (this.fireButton.isDown)
        {
          this.fireBullet();
        }

        if (this.game.time.now > this.firingTimer)
        {
          this.enemyFires();
        }

        //  Run collision
        this.game.physics.arcade.overlap(this.bullets, this.aliens, this.collisionHandler, null, this);
        this.game.physics.arcade.overlap(this.enemyBullets, this.player, this.enemyHitsPlayer, null, this);
    }

}

 render = () => {

    // for (var i = 0; i < this.aliens.length; i++)
    // {
    //     this.game.debug.body(this.aliens.children[i]);
    // }

}

 collisionHandler (bullet, alien) {
    this.explosionSound.play();
    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();

    //  Increase the score
    this.score += 20;
    this.scoreText.text = this.scoreString + this.score;

    //  And create an explosion :)
    var explosion = this.explosions.getFirstExists(false);
    explosion.reset(alien.body.x, alien.body.y);
    explosion.play('kaboom', 30, false, true);

    if (this.aliens.countLiving() == 0)
    {
      this.score += 1000;
      this.scoreText.text = this.scoreString + this.score;

       this.enemyBullets.callAll('kill',this);
       this.stateText.text = " You Won, \n Click to restart";
       this.stateText.visible = true;
        
        clearInterval(this.keepFireBulletInterval);
        clearInterval(this.keepPlayerMovingInterval);
        this.level ++;
        //the "click to restart" handler
        this.game.input.onTap.addOnce(this.restart,this);
    }

}

 enemyHitsPlayer (player,bullet) {
    
    bullet.kill();

    let live = this.lives.getFirstAlive();

    if (live)
    {
        clearInterval(this.keepFireBulletInterval);
        clearInterval(this.keepPlayerMovingInterval);
        live.kill();
    }

    //  And create an explosion :)
    var explosion = this.explosions.getFirstExists(false);
    explosion.reset(player.body.x, player.body.y);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (this.lives.countLiving() < 1)
    {
        player.kill();
       this.enemyBullets.callAll('kill');

       this.stateText.text=" GAME OVER \n Click to restart";
       this.stateText.visible = true;
       
        //the "click to restart" handler
        this.game.input.onTap.addOnce(this.restart,this);
    }

}

 enemyFires () { 

    //  Grab the first bullet we can from the pool
    let enemyBullet = this.enemyBullets.getFirstExists(false);

    this.livingEnemies.length=0;
    let self = this;
    this.aliens.forEachAlive(function(alien){

        // put every living enemy in an array
        self.livingEnemies.push(alien);
    });


    if (enemyBullet && this.livingEnemies.length > 0)
    {
        
        var random=this.game.rnd.integerInRange(0,this.livingEnemies.length-1);

        // randomly select one of them
        var shooter=this.livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet.reset(shooter.body.x, shooter.body.y);

        this.game.physics.arcade.moveToObject(enemyBullet,this.player,(120+ (this.level*10)));
        this.firingTimer = this.game.time.now + (2000- (this.level*10));
    }

}

 fireBullet () {
    this.fireSound.play();
    //  To avoid them being allowed to fire too fast we set a time limit
    if (this.game.time.now > this.bulletTime)
    {
        //  Grab the first bullet we can from the pool
        let bullet =this.bullets.getFirstExists(false);

        if (bullet)
        {
           
            //  And fire it
            bullet.reset(this.player.x, this.player.y + 8);
            bullet.body.velocity.y = -400;
            this.bulletTime = this.game.time.now + 200;
        }
    }

}

 resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}

 restart () {

    //  A new level starts
    //resets the life count
    this.lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    this.aliens.removeAll();
    this.createAliens();

    //revives the player
    this.player.revive();
    //hides the text
    this.stateText.visible = false;

}
/**
 * @description
 * Player fire one bullet
 */
clickFireBullet(){
  this.fireBullet();
}
/**
 * @description
 * Player keep firing bullet if click first time and stop keep firing next time 
 */
keepFireBullet(){
    this.keepFireing = ! this.keepFireing;
    if(this.keepFireing){
        this.keepFireBulletInterval = setInterval( ()=> {
            this.fireBullet();
        }, 300);
    }else{
        clearInterval(this.keepFireBulletInterval);
    }  
}

/**
 * @description
 * Move player left and right
 * @param isLeft => if need to move left true else false 
 */
playerMove(isLeft, stop= false){
        this.playerIsMoveLeft = isLeft;
        clearInterval(this.keepPlayerMovingInterval);
        if(! stop){
            if (isLeft){
                //this.keepPlayerMovingInterval = setInterval( ()=> { this.player.body.velocity.x = -200; }, 70);
                this.player.body.velocity.x = -400;
            }else {
                //this.keepPlayerMovingInterval = setInterval( ()=> { this.player.body.velocity.x = 200; }, 70);
                this.player.body.velocity.x = 400;
            }
        }
}
/**
 * @description
 * Call when player out of screen
 */
playerOutOfBounds(player) {
    //  Move the player to the left of the screen again
    if(this.playerIsMoveLeft){
        this.player.reset(this.player.x, 0);
    }else{
        this.player.reset(this.player.x, this.game.world.width);   
    }
    
    this.player.reset(this.player.y, this.game.world.height-50);
}


}

